const express=require('express');
const path=require('path');
const app=express();
const port=process.env.PORT || 1000;
const host=process.env.BASE_URL || "http://localhost:1000";

const demo=require("./routes/demo");


app.use(express.static(path.join(__dirname,"public")));
app.use("/",express.static(path.join(__dirname,"public")));


app.use("/api/demo",demo);





app.listen(port, ()=>console.log(`Listening at ${host}`));