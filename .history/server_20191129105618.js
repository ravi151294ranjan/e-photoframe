const express=require('express');
const path=require('path');
const app=express();
const mongoose=require('mongoose');
const port=process.env.PORT || 1000;
const host=process.env.BASE_URL || "http://localhost:1000";



//DB Connectivity
mongoose.connect(
    "mongodb://localhost:27017/PhotoframeDB",
    {useNewUrlParser:true},
    err=>{
        if(err) throw err.message;
        console.log("mongodb connection successfully");
    },
);



//routes files
const demo=require("./routes/demo");



//middleware
app.use(express.static(path.join(__dirname,"public")));
app.use("/",express.static(path.join(__dirname,"public")));


app.use("/api/demo",demo);





app.listen(port, ()=>console.log(`Listening at ${host}`));