import { Component, OnInit } from '@angular/core';
import { DemoService } from '../DemoServices/demo.service';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {

  constructor(private demoService: DemoService) { }

  ngOnInit() {
    this.demoService.Result.subscribe(result => {
      if (!result) {
        return;
      }
    });
  }

}
