import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LanguageTranslationModule } from './shared/modules/language-translation/language-translation.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { DemoComponent } from './demo/demo.component';
import {DemoService} from './DemoServices/demo.service';
import {LoginModule} from './login/login.module';
import { from } from 'rxjs';
import { AuthServiceConfig} from 'angularx-social-login';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        LanguageTranslationModule,
        AppRoutingModule,
        LoginModule
    ],
    declarations: [AppComponent, DemoComponent],
    providers: [AuthGuard, DemoService, AuthServiceConfig],
    bootstrap: [AppComponent]
})
export class AppModule {}
