import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import {SocialLoginModule, AuthServiceConfig, FacebookLoginProvider, GoogleLoginProvider} from 'angularx-social-login';
import {Observable, Subscribable} from 'rxjs';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { from } from 'rxjs';


const config = new AuthServiceConfig([
    {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider('')
    },
    {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider('')
    }

]);
@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        LoginRoutingModule,
        SocialLoginModule,
        Observable,
    ],
    declarations: [LoginComponent]
})
export class LoginModule {}
