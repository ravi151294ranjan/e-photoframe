import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import {SocialLoginModule, AuthServiceConfig, FacebookLoginProvider, GoogleLoginProvider} from 'angularx-social-login';
import {HttpClientModule} from '@angular/common/http';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import {LoginService} from './services/login.service';
import { from } from 'rxjs';


const config = new AuthServiceConfig([
    {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider('2106164059690571')
    },
    {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider('892107756553-tee3pov1f6pep2m8no1auhogb7p7do4h.apps.googleusercontent.com')
    }

]);

export function provideConfig() {
    return config;
  }

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        LoginRoutingModule,
        SocialLoginModule,
        HttpClientModule
    ],
    declarations: [LoginComponent],
    providers: [
        {provide: AuthServiceConfig, useFactory: provideConfig},
        LoginService
      ],
})
export class LoginModule {}
