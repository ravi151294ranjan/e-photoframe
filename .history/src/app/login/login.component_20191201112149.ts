import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { AuthService } from 'angularx-social-login';
import {FacebookLoginProvider, GoogleLoginProvider, SocialUser } from 'angularx-social-login';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    response;
    socialusers = new SocialUser();
    SocialloginService: any;
    constructor(
      public router: Router,
      public OAuth: AuthService
    ) {}

    ngOnInit() {}

    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }

    public socialSignIn(socialProvider: string) {
        let socialPlatformProvider ;
        if (socialProvider === 'facebook') {
          socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
        } else if (socialProvider === 'google') {
          socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
        }
        this.OAuth.signIn(socialPlatformProvider).then(socialusers => {
            console.log(socialProvider, socialusers);
            console.log(socialusers);
            this.router.navigate([`/dashboard`]);
          //  this.Savesresponse(socialusers);
          });
    }
    // Savesresponse(socialusers: SocialUser) {
    //     this.SocialloginService.Savesresponse(socialusers).subscribe((res: any) => {
    //       // tslint:disable-next-line:no-debugger
    //       debugger;
    //       console.log(res);
    //       this.socialusers = res;
    //       this.response = res.userDetail;
    //       localStorage.setItem('socialusers', JSON.stringify( this.socialusers));
    //       console.log(localStorage.setItem('socialusers', JSON.stringify(this.socialusers)));
    //       this.router.navigate([`/dashboard`]);
    //     });
    //   }
}
