import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  getAPIData() {
    return this.http.get('https://jsonplaceholder.typicode.com/users');
  }

  postAPIData() {
    return this.http.post('http://locahost:1000/api/login/', {'firstName': 'Ravi'});
  }

}
