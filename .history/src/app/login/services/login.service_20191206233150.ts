import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../../auth/user';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  getAPIData() {
    return this.http.get('https://jsonplaceholder.typicode.com/users');
  }

  postAPIData() {
    return this.http.post('http://localhost:1000/api/login', {' ' : ' '});
  }

}
