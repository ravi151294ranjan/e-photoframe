import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import {AuthService, FacebookLoginProvider, GoogleLoginProvider, SocialUser} from 'angularx-social-login';
import {Observable, Subscribable} from 'rxjs';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    public user: any = SocialUser;
    loggedIn: boolean;

    constructor(
      public router: Router, private authService: AuthService
    ) {}

    ngOnInit() {
    }

    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }

    facebookLogin() {
        this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then((userData) => {
            alert(JSON.stringify(userData));
            this.user = userData;
        });
    }

    googleLogin() {
        this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    }
}
