import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { AuthService } from 'angularx-social-login';
import {FacebookLoginProvider, GoogleLoginProvider, SocialUser } from 'angularx-social-login';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    response;
    socialusers = new SocialUser();
   // SocialloginService: any;
    constructor(
      public router: Router,
      public OAuth: AuthService
    ) {}

    ngOnInit() {}

    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }

    public facebookLogin() {
        this.OAuth.signIn(FacebookLoginProvider.PROVIDER_ID).then(socialusers => {
            console.log(socialusers);
           // localStorage.setItem('socialusers', 'true');
           // this.router.navigate(['/dashboard']);
          //  this.Savesresponse(socialusers);
          if (socialusers) {
           // localStorage.setItem('isLoggedin', 'true');
            return true;
          }
          });
    }

    public googleLogin() {
      this.OAuth.signIn(GoogleLoginProvider.PROVIDER_ID).then(socialusers => {
          console.log(socialusers);
          localStorage.setItem('socialusers', 'true');
         // this.router.navigate(['/dashboard']);
         // this.Savesresponse(socialusers);
        });
  }
    // Savesresponse(socialusers: SocialUser) {
    //     this.SocialloginService.Savesresponse(socialusers).subscribe((res: any) => {
    //       // tslint:disable-next-line:no-debugger
    //       debugger;
    //       console.log(res);
    //       this.socialusers = res;
    //       this.response = res.userDetail;
    //       localStorage.setItem('socialusers', JSON.stringify( this.socialusers));
    //       console.log(localStorage.setItem('socialusers', JSON.stringify(this.socialusers)));
    //       this.router.navigate([`/dashboard`]);
    //     });
    //   }
}
