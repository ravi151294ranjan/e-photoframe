import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import {AuthService, FacebookLoginProvider, GoogleLoginProvider, SocialUser} from 'angularx-social-login';
import {Observable, Subscribable} from 'rxjs';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    user: SocialUser;
    loggedIn: boolean;

    constructor(
      public router: Router, private authService: AuthService
    ) {}

    ngOnInit() {
        this.authService.authState.subscribe(this.user); =>{
            this.user = this.user ;
            this.loggedIn = (this.user != null);
            console.log(this.user);
        }
    }

    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }

    facebookLogin() {}

    googleLogin() {}
}
