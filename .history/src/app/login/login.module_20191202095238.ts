import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import {SocialLoginModule, AuthServiceConfig, FacebookLoginProvider, GoogleLoginProvider} from 'angularx-social-login';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { from } from 'rxjs';


const config = new AuthServiceConfig([
    {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider('2106164059690571')
    },
    {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider('511826885402-1rv595hbkvboda7k0naa00hobnvssmng.apps.googleusercontent.com')
    }

]);

export function provideConfig() {
    return config;
  }

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        LoginRoutingModule,
        SocialLoginModule,
    ],
    declarations: [LoginComponent],
    providers: [
        {provide: AuthServiceConfig, useFactory: provideConfig}
      ],
})
export class LoginModule {}
