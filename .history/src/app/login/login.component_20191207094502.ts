import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { AuthService } from 'angularx-social-login';
import {FacebookLoginProvider, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { LoginService } from './services/login.service';
import {} from '../auth/user';
    import { from } from 'rxjs';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    response;
    socialusers = new SocialUser();
   // SocialloginService: any;
    constructor(
      public router: Router,
      public OAuth: AuthService,
      public loginService: LoginService,
    ) {}

    ngOnInit() {
      this.loginService.getAPIData().subscribe((response) => {
        console.log('response is', response);
    }, (error) => {
        console.log('error is', error);
    });

    this.loginService.postAPIData().subscribe((response) => {
        console.log('response from post is' , response);
    }, (error) => {
        console.log('error during post is' , error );
    });
    }

    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
        this.ngOnInit();
    }

    public facebookLogin() {
        this.OAuth.signIn(FacebookLoginProvider.PROVIDER_ID).then(socialusers => {
            console.log(socialusers);
           // localStorage.setItem('socialusers', 'true');
          //  this.Savesresponse(socialusers);
          if (socialusers) {
           localStorage.setItem('isLoggedin', 'true');
           this.router.navigate(['/dashboard']);
           this.ngOnInit();
           // return true;
          }
          });
    }

    public googleLogin() {
      this.OAuth.signIn(GoogleLoginProvider.PROVIDER_ID).then(socialusers => {
          console.log(socialusers);
        //  localStorage.setItem('socialusers', 'true');
         // this.router.navigate(['/dashboard']);
         // this.Savesresponse(socialusers);
         if (socialusers) {
            localStorage.setItem('isLoggedin', 'true');
            this.router.navigate(['/dashboard']);
            this.ngOnInit();
            // return true;
           }
        });
  }
    // Savesresponse(socialusers: SocialUser) {
    //     this.SocialloginService.Savesresponse(socialusers).subscribe((res: any) => {
    //       // tslint:disable-next-line:no-debugger
    //       debugger;
    //       console.log(res);
    //       this.socialusers = res;
    //       this.response = res.userDetail;
    //       localStorage.setItem('socialusers', JSON.stringify( this.socialusers));
    //       console.log(localStorage.setItem('socialusers', JSON.stringify(this.socialusers)));
    //       this.router.navigate([`/dashboard`]);
    //     });
    //   }
}
