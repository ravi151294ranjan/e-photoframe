import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { DeviceService } from './srvices/device.service';


@Component({
    selector: 'app-adddevice',
    templateUrl: './adddevice.component.html',
    styleUrls: ['./adddevice.component.scss'],
    animations: [routerTransition()]
})
export class AddDeviceComponent implements OnInit {
    constructor(private deviceService: DeviceService) {}

    ngOnInit() {

        this.deviceService.getAPIData().subscribe((response) => {
            console.log('response is', response);
        }, (error) => {
            console.log('error is', error);
        });

        this.deviceService.postAPIData().subscribe((response) => {
            console.log('response from post is' , response);
        }, (error) => {
            console.log('error during post is' , error );
        });
    }
}
