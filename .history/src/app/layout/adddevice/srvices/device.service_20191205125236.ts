import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  constructor(private http: HttpClient) { }

  getAPIData() {
    return this.http.get('/getData');
  }

  postAPIData() {
    return this.http.post('/api/postData', {'': ''});
  }
}
