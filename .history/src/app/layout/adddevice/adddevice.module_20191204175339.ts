import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import {AdddeviceRoutingModule} from './adddevice-routing.module';
import {AddDeviceComponent} from './adddevice.component';
    import { from } from 'rxjs';

@NgModule({
    imports: [CommonModule, AdddeviceRoutingModule, PageHeaderModule],
    declarations: [AddDeviceComponent]
})
export class AdddeviceModule {}
