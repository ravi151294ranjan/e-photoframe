import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdddeviceRoutingModule } from './adddevice-routing.module';
import { AddDeviceComponent } from './adddecive.component';
import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [CommonModule, AdddeviceRoutingModule, PageHeaderModule],
    declarations: [AddDeviceComponent]
})
export class AdddeviceModule {}
