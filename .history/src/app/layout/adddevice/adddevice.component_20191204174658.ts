import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
    selector: 'app-adddevice',
    templateUrl: './adddevice.component.html',
    styleUrls: ['./adddevice.component.scss'],
    animations: [routerTransition()]
})
export class GridComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
}
