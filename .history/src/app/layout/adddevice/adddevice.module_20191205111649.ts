import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import {AdddeviceRoutingModule} from './adddevice-routing.module';
import {AddDeviceComponent} from './adddevice.component';
import {HttpClientModule} from '@angular/common/http';
    import { from } from 'rxjs';

@NgModule({
    imports: [CommonModule, AdddeviceRoutingModule, PageHeaderModule, HttpClientModule],
    declarations: [AddDeviceComponent]
})
export class AdddeviceModule {}
