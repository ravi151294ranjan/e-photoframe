import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
    selector: 'app-adddevice',
    templateUrl: './adddevice.component.html',
    styleUrls: ['./adddevice.component.scss'],
    animations: [routerTransition()]
})
export class AddDeviceComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
}
