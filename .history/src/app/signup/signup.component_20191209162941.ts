import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { routerTransition } from '../router.animations';
import {SignupService} from './services/signup.service';
import {User} from '../auth/user';
import {newUser} from './signupmodel/newuser';
import { from } from 'rxjs';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    constructor(
        public signupservice: SignupService,
        public router: Router
    ) {}

    newuser = new newUser();
    objPost: newUser;

    ngOnInit() {}

    onSignup() {

        this.signupservice.postUserAPIData(this.newuser).subscribe((response) => {
            this.objPost = response;
        }, (error) => {
            console.log('error is ' + error );
        });
        this.router.navigate(['/login']);

    }
}
