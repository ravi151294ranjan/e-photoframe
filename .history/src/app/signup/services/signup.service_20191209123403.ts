import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../../auth/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(private http: HttpClient) { }

  postUserAPIData(user: User): Observable<any> {
    return this.http.post('http://localhost:1000/api/signup', user );
  }
}
