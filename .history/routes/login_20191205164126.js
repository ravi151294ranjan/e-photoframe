const express=require('express');
const router=express.Router();



router.get("/",(req,res)=>{
    res.send("Demo ApI is working");
})

router.post("/",(req,res)=>{
    console.log("hello");
    res.send("Post request received!!");
})

router.put("/:id",(req,res)=>{
    res.send(`${req.params.id} received to edit`);
})

router.delete("/:id",(req,res)=>{
    res.send(`${req.params.id} received to delete`)
})


module.exports=router;