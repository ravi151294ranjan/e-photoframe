import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';

import { AdddeviceRoutingModule } from './adddevice-routing.module';
import { AddDeviceComponent } from './adddecive.component';
import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [CommonModule, AdddeviceRoutingModule, PageHeaderModule, HttpClientModule],
    declarations: [AddDeviceComponent]
})
export class AdddeviceModule {}
