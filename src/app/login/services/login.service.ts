import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {loginUser} from '../loginmodel/logindetails';
import { Observable, BehaviorSubject, from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  // getAPIData(): Observable<any> {
  //   return this.http.get('https://jsonplaceholder.typicode.com/users');
  // }

  postAPIData(user: loginUser): Observable<any> {
    return this.http.post('http://localhost:1000/api/login', user );
  }

}
