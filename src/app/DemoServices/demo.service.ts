import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DemoService {

  constructor(private _http: HttpClient) { }
  get Result(): Observable<any> {
    return this._http.get('http://localhost:1000/api/demo');
  }
}
