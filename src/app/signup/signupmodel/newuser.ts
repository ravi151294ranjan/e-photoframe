// tslint:disable-next-line:class-name
export class newUser {
    id: string;
    firstName: string;
    email: string;
    lastName: string;
    password: string;
    repeatPassword: string;
}
