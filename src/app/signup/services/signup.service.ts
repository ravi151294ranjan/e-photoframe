import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {newUser} from '../signupmodel/newuser';
import { Observable, from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(private http: HttpClient) { }

  postUserAPIData(user: newUser): Observable<any> {
    return this.http.post('http://localhost:1000/api/signup', user );
  }
}
