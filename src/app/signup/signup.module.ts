import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import {SignupService} from './services/signup.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { from } from 'rxjs';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SignupRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  declarations: [SignupComponent],
  providers: [SignupService]
})
export class SignupModule { }
